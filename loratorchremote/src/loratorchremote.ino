// rf95_reliable_datagram_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, reliable messaging client
// with the RHReliableDatagram class, using the RH_RF95 driver to control a RF95 radio.
// It is designed to work with the other example rf95_reliable_datagram_server
// Tested with Anarduino MiniWirelessLoRa, Rocket Scream Mini Ultra Pro with the RFM95W
#include <RHReliableDatagram.h>
#include <RH_RF95.h>
#include <SPI.h>
#include <wifi.h>

#define CLIENT_ADDRESS 1
#define SERVER_ADDRESS 2

RH_RF95 driver(D8, D2); //RH_RF95 driver(5, 2); // Rocket Scream Mini Ultra Pro with the RFM95W
RHReliableDatagram manager(driver, CLIENT_ADDRESS);
int incomingByte = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial) ; // Wait for serial port to be available
  Serial.println("serial ok");

  if (manager.init()) {
    Serial.println("trx init ok");
  } else {
    Serial.println("trx init failed");
  }
  driver.setFrequency(868.0);

  delay(10);
  connectToWiFiAP(SSID, PASS);
}

void sendData(String input) {
  char data[sizeof(input)];
  input.toCharArray(data, sizeof(data));

  Serial.printf("Sending data: %s\n", &data);
  if (manager.sendtoWait((uint8_t *)data, strlen(data), SERVER_ADDRESS)) {
    Serial.println("Successfully sent data");
  } else {
    Serial.println("sendtoWait failed");
  }
}

long lastSent = 0L;

void loop() {
  listenForHttpMsgs([](String output){
    Serial.println(String("Sending command to flashlight: ") + output);
    sendData(output);
  });

  if (millis() - lastSent > 5000) { //if 5 secs have passed
    reportHealth("192.168.20.106", "flashlight-transceiver");
    lastSent = millis();
  }
}
