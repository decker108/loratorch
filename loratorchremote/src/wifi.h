#ifndef wifi_h
#define wifi_h

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

ESP8266WiFiMulti WiFiMulti;

const uint16_t port = 8080;
const char * host = "192.168.0.241"; // ip or hostname

WiFiServer server(8080);

void sendMessage(char* host, char sendingStation, int isOk) {
    // Use WiFiClient class to create TCP connections
    WiFiClient client;

    if (!client.connect(host, port)) {
        Serial.println("connection failed");
        Serial.println("wait 5 sec...");
        delay(5000);
        return;
    }

    // We now create a URI for the request
    String url = "/service/sending-station/"; //http://localhost:8080/service/sending-station/

    Serial.print("Requesting URL: ");
    Serial.println(url);

    String body = String("{\"station\":") + sendingStation + ",\"ok\":\"" + (isOk == 1 ? "true" : "false") + "\"}";
    int contentLength = body.length();

    // This will send the request to the server
    client.print(String("POST ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: close\r\n" +
                 "Content-Type: application/json\r\n" +
                 "Content-Length: " + contentLength + "\r\n");
    client.print("\r\n"); //this newline means end of headers and start of body
    client.print(body);

    unsigned long timeout = millis();
    while (client.available() == 0) {
        if (millis() - timeout > 5000) {
            Serial.println(">>> Client Timeout !");
            client.stop();
            return;
        }
    }

    // Read all the lines of the reply from server and print them to Serial
    while(client.available()){
        String line = client.readStringUntil('\r');
        Serial.print(line);
    }

    Serial.println("closing connection");
    client.stop();
}

void listenForHttpMsgs(std::function<void(String)> callbackFn) {
    WiFiClient client = server.available();
    if (!client) {
        return;
    }

    // Wait until the client sends some data
    Serial.println("new client");
    while(!client.available()){
        delay(1);
    }

    // Read the first line of the request
    String req = client.readStringUntil('\r');
    Serial.println(req);
    client.flush();

    // Match the request
    int val;
    if (req.indexOf("/flashlight/on") != -1) {
      val = 1;
      callbackFn("255");
    } else if (req.indexOf("/flashlight/off") != -1) {
      val = 0;
      callbackFn("0");
    } else {
      Serial.println("invalid request");
      client.stop();
      return;
    }

    client.flush();

    // Prepare the response
    String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nFlashlight is now ";
    s += (val)?"on":"off";
    s += "</html>\n";

    // Send the response to the client
    client.print(s);
    delay(1);
    Serial.println("Client disconnected");
}

void connectToWiFiAP(const char* SSID, const char* PASS) {
  WiFi.hostname("flashlight_transceiver");

  WiFi.begin(SSID, PASS);

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");

  while(WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");

  server.begin();

  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

String calculateHealth() {
    int strength = WiFi.RSSI();
    boolean healthy = strength > -70;
    String out = "&healthy=";
    out += String(healthy);
    out += "&msg=";
    out += healthy ? "OK": "Signal strength is low (" + String(strength) + ")";
    return out;
}

void reportHealth(String watchDogIp, String deviceId) {
  Serial.println("Begin report health");
  Serial.println("Wifi-status: " + String(WiFi.status()) + " " + String(WiFi.RSSI()));

  HTTPClient http;
  http.setTimeout(1000);
  http.begin("http://" + String(watchDogIp) + ":8050/service/report");
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  http.POST("from=" + String(deviceId) + calculateHealth());
  http.writeToStream(&Serial);
  http.end();
  Serial.println("Complete report health");
}

#endif
