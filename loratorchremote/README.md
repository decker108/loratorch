# HOWTO

0. Clone the repo.
1. Setup a pio env with ```docker pull eclipse/platformio``` and ```docker run -t -i --device=/dev/ttyUSB0 eclipse/platformio bash```
2. Initialize the pio project by running ```platformio init -b wemos``` in the project root.
3. Compile code with ```platformio run```
4. Upload code with ```platformio run -t upload --upload-port /dev/ttyUSB0```

For monitoring the code on the device, use ```platformio serialports monitor``` and choose /dev/ttyUSB0 in the menu.
