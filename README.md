# loratorch
Flashlight controlled by a lora radio module

See readmes in each subproject:
https://github.com/Decker108/loratorch/blob/master/loratorchrx/README.md
https://github.com/Decker108/loratorch/blob/master/loratorchremote/README.md
